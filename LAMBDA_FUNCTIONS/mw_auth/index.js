var jwt = require('jsonwebtoken');
const JWT_KEY = "TESTKEYMW927897#&*@Ui"

exports.handler = async (event) => {

  const VALID_LOGINS = [{
    "user_id": "1",
    "credentials": "admin:testadmin12345",
    "name": "Test Login",
    "thumbnail": "pp.png"
  }]

  var response = {
    statusCode: 400,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST"
    },
    body: {
      error: "Invalid Request",
      error_code: 301,
      user_details: {},
      token: ""
    }
  };

  //Extract request body from payload
  let credentials = {}
  try {
    credentials = JSON.parse(event.body)
  } catch (e) {
    response.body = JSON.stringify(response.body)
    return response;
  }

  //Credentials not passed
  if (!credentials.username || !credentials.password) {
    response.body = JSON.stringify(response.body)
    return response;
  }


  //Valid request
  response.statusCode = 200

  let user_details = VALID_LOGINS.find(u => `${credentials.username}:${credentials.password}` == u.credentials)
  if (!user_details) {
    //Invalid credentials
    response.body.error = "Invalid login details"
    response.body.error_code = 301
    response.body = JSON.stringify(response.body)
    return response;
  }

  //Valid credentials
  response.body.error = "Success"
  response.body.error_code = 0

  //Expires after 24 hours
  response.body.token = jwt.sign({
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
    data: user_details
  }, JWT_KEY);

  delete user_details.credentials
  response.body.user_details = user_details


  response.body = JSON.stringify(response.body)
  return response;

};
