var jwt = require('jsonwebtoken');
const JWT_KEY = "TESTKEYMW927897#&*@Ui"

exports.handler = async (event) => {

  var response = {
    statusCode: 400,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,GET"
    },
    body: {
      error: "Invalid Request",
      error_code: 301,
      campaigns: [],
      token: ""
    }
  };

  let token = event.headers.AuthToken ? event.headers.AuthToken : event.headers.authtoken
  if (!token) {
    response.body.error = "Missing token"
    response.body.error_code = 302
    response.body = JSON.stringify(response.body)
    return response;
  }

  const CAMPAIGNS_DB = [{
      thumbnail: "b1.png",
      description: "UA Sports Singapore - 3 Billboards",
      search_keywords: "UA Sports Singapore",
      status: 1,
      modification_count: 0,
      duration: {
        start: "2019-04-01",
        end: "2019-04-31"
      }
    },
    {
      thumbnail: "b2.png",
      description: "Nespresso",
      search_keywords: "Nespresso",
      status: 2,
      modification_count: 0,
      duration: {
        start: "2019-05-26",
        end: "2019-06-20"
      }
    },
    {
      thumbnail: "b3.png",
      description: "DBS - 3 Billboards",
      search_keywords: "DBS - 3 Billboards",
      status: 2,
      modification_count: 0,
      duration: {
        start: "2019-06-10",
        end: "2019-06-30"
      }
    },
    {
      thumbnail: "b4.png",
      description: "Test Campaign",
      search_keywords: "Test Campaign",
      status: 2,
      modification_count: 0,
      duration: {
        start: "2019-07-12",
        end: "2019-07-30"
      }
    },
    {
      thumbnail: "b5.png",
      description: "Focus Media",
      search_keywords: "Focus Media",
      status: 3,
      modification_count: 4,
      duration: {
        start: "2019-08-01",
        end: "2019-08-31"
      }
    },
    {
      thumbnail: "b6.jpeg",
      description: "UA Sports Singapore - 3 Billboards",
      search_keywords: "UA Sports Singapore",
      status: 3,
      modification_count: 0,
      duration: {
        start: "2019-07-20",
        end: "2019-08-11"
      }
    },
    {
      thumbnail: "b1.png",
      description: "Orchard - 3 Billboards",
      search_keywords: "Orchard",
      status: 3,
      modification_count: 0,
      duration: {
        start: "2019-06-15",
        end: "2019-07-15"
      }
    },
    {
      thumbnail: "b2.png",
      description: "Rendezvous - 3 Billboards",
      search_keywords: "Rendezvous",
      status: 4,
      modification_count: 0,
      duration: {
        start: "2019-03-03",
        end: "2019-03-31"
      }
    },
    {
      thumbnail: "b3.png",
      description: "Orchard - 3 Billboards",
      search_keywords: "Orchard",
      status: 3,
      modification_count: 0,
      duration: {
        start: "2019-06-15",
        end: "2019-07-15"
      }
    },
    {
      thumbnail: "b4.png",
      description: "Orchard - 3 Billboards",
      search_keywords: "Orchard",
      status: 3,
      modification_count: 0,
      duration: {
        start: "2019-06-15",
        end: "2019-07-15"
      }
    },
    {
      thumbnail: "b4.png",
      description: "Orchard - 3 Billboards",
      search_keywords: "Orchard",
      status: 3,
      modification_count: 0,
      duration: {
        start: "2019-06-15",
        end: "2019-07-15"
      }
    }
  ]

  response.statusCode = 200
  let user_details = null
  //Check if token is valid 
  try {
    user_details = jwt.verify(token, JWT_KEY)

    if (Math.floor(Date.now() / 1000) > user_details.exp) {
      //token expired
      response.body.error = "Expired token"
      response.body.error_code = 303
      response.body = JSON.stringify(response.body)
      return response;
    }

    user_details = user_details.data

  } catch (err) {
    response.body.error_code = 304
    response.body.error = "Something went wrong while verifying token"
    response.body = JSON.stringify(response.body)
    return response;
  }

  response.body.error = "Success"
  response.body.error_code = 0

  let campaigns = CAMPAIGNS_DB
  if (event.queryStringParameters) {
    let query_url = event.queryStringParameters
    if (query_url.status) {
      campaigns = campaigns.filter(c => c.status == query_url.status)
    }

    if (query_url.search) {
      campaigns = campaigns.filter(c => c.description.toLowerCase().indexOf(query_url.search.toLowerCase()) > -1)
    }
  }
  response.body.campaigns = campaigns


  response.body = JSON.stringify(response.body)
  return response;

};
