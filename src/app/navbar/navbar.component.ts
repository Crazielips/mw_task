import { Component, OnInit } from '@angular/core';
import { LoginService, user_info } from '../services/login/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  user_details : user_info = null
  
  constructor(
    private loginservice : LoginService
    ) { 
      this.user_details = this.loginservice.userDetails()
    }
    
    ngOnInit() {
    }
    
    logMeOut() : void {
      this.loginservice.logUserOut()
    }
    
  }
  