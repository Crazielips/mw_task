import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {
  
  constructor(
    private router : Router,
    private loginservice : LoginService
    ) {
      
    }
    
    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        let url = state.url.toLowerCase()
        if (url[0] === '/') {
          url = url.substring(1)
        }
        let logged_in = this.loginservice.isLoggedIn()
        if (url == "") {
          //login page
          if (logged_in) {
            this.router.navigateByUrl("/dashboard/campaigns")          
          }
          return !logged_in
        }
        if (!logged_in) {
          this.router.navigateByUrl("/")          
        }
        return logged_in
      }
      
    }
    