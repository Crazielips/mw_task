import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

export interface user_info {
  token : string,
  name : string,
  user_id : string,
  username : string,
  thumbnail : string,
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  token_label = "___"
  user_info : user_info = null
  
  constructor(
    private router : Router
    ) { }
    
    isLoggedIn() : boolean {
      let ud : string = sessionStorage.getItem(this.token_label)
      if (!ud || ud == "") {
        ud = localStorage.getItem(this.token_label)
      }
      if (!ud || ud == "") {
        return false
      }
      this.user_info = JSON.parse(ud)
      if (!this.user_info.token || this.user_info.token == "") {
        return false
      }
      return true
    }
    
    logUserIn(user_details : user_info, remember_user : boolean = false) : void {
      if (remember_user) {
        localStorage.setItem(this.token_label, JSON.stringify(user_details))
      }else {
        sessionStorage.setItem(this.token_label, JSON.stringify(user_details))
      }
      this.router.navigateByUrl("dashboard/campaigns")
    }
    
    logUserOut() : void {
      localStorage.removeItem(this.token_label)
      sessionStorage.removeItem(this.token_label)
      this.router.navigateByUrl("/")
    }
    
    userDetails() : user_info {
      if (!this.isLoggedIn()) {
        return null
      }
      return this.user_info
    }
    
  }
  