import { Injectable } from '@angular/core';
import { user_info } from '../login/login.service';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  
  API_ENDPOINT = "https://4vf82note0.execute-api.ap-southeast-1.amazonaws.com"
  API_STAGE = this.API_ENDPOINT + "/sandbox/"
  CONFIG = {
    api_endpoints : {
      login : this.API_STAGE + "login",
      campaigns : this.API_STAGE + "campaigns"
    }
  }
  
  constructor() { }
  
  prepareHeader(userinfo: user_info): HttpHeaders {
    return new HttpHeaders({
      'AuthToken': userinfo.token,
    })
  }
  
}
