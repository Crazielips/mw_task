import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CampaignsComponent } from './dashboard/campaigns/campaigns.component';
import { LoginComponent } from './login/login.component';
import { AuthguardGuard } from './services/login/authguard.guard';


const routes: Routes = [
  { path : "", component : LoginComponent, canActivate: [AuthguardGuard] },
  { path : "dashboard", component : DashboardComponent, canActivate: [AuthguardGuard]  },
  { path : "dashboard/:sub_page", component : DashboardComponent, canActivate: [AuthguardGuard]  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
