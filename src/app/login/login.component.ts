import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ConfigService } from '../services/config/config.service';
import { LoginService } from '../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  processing = false
  err_timer : any
  credentials = {
    username : "",
    password : "",
    remember_me : 0,
    errors : []
  }
  
  constructor(
    private http : HttpClient,
    private config : ConfigService,
    private loginservice : LoginService
    ) { }
    
    ngOnInit() {
    }
    
    logMeIn() : void {
      this.credentials.errors = []
      if (this.credentials.username.trim() == "" || this.credentials.password.trim() == "") {
        this.credentials.errors.push("incomplete_params")
        this.__errorTimerHandler()
        return
      }
      
      this.processing = true
      this.http.post<any>(this.config.CONFIG.api_endpoints.login, { username : this.credentials.username, password : this.credentials.password})
      .subscribe((response) => {
        if (response.error_code > 0 || response.token == "") {
          this.processing = false
          this.credentials.errors.push("invalid_credentials")
          this.__errorTimerHandler()
          return
        }
        //Login successful
        
        this.loginservice.logUserIn({
          token : response.token,
          username: this.credentials.username,
          name: response.user_details.name,
          user_id: response.user_details.user_id,
          thumbnail: response.user_details.thumbnail,
        }, this.credentials.remember_me == 1)
        
      },
      (e : HttpErrorResponse) => {
        this.processing = false
        this.credentials.errors.push("501_error")
        this.__errorTimerHandler()
        console.log(e)
      })
    }
    
    __errorTimerHandler(duration = 3000) : void {
      clearTimeout(this.err_timer)
      this.err_timer = setTimeout(() => {
        this.credentials.errors = []
      }, duration)
    }
    
  }
  