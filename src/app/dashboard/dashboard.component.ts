import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService, user_info } from '../services/login/login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  
  opened = true
  sub_page_showing = ""
  user_details : user_info = null
  
  constructor(
    private router : Router,
    private loginservice : LoginService,
    private activatedRoute : ActivatedRoute
    ) { 
      this.user_details = this.loginservice.userDetails()
    }
    
    ngOnInit() {
      this.setPageActivePage()
    }
    
    setPageActivePage() : void {
      let sub_page_showing = this.activatedRoute.snapshot.queryParams["sub_page"];
      if (!sub_page_showing && this.activatedRoute.snapshot.url.length >= 2) {
        sub_page_showing = this.activatedRoute.snapshot.url[1].path
      }      
      this.sub_page_showing = !sub_page_showing ? "" : sub_page_showing
    }
    
    gotoPage(toWhere : string, prefix : string = "dashboard/") : void {
      this.router.navigateByUrl(prefix + toWhere)
      this.sub_page_showing = toWhere
      window.scroll(0,0)
    }
  }
  