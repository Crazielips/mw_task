import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerformancesummaryComponent } from './performancesummary.component';

describe('PerformancesummaryComponent', () => {
  let component: PerformancesummaryComponent;
  let fixture: ComponentFixture<PerformancesummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerformancesummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformancesummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
