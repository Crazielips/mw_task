import { Component, OnInit } from '@angular/core';
import { user_info, LoginService } from 'src/app/services/login/login.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config/config.service';


interface CAMPAIGNS {
  thumbnail : string,
  description : string,
  search_keywords : string,
  duration : CAMPAIGNS_DURATION ,
  status : number, //1 = Published, 2 = Processing, 3 = On going, 4 = Archived
  statusText ?: string,
  modification_count : number
}

interface CAMPAIGNS_DURATION {
  end : string,
  start : string,
}

@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.scss']
})
export class CampaignsComponent implements OnInit {
  
  search_campaign_txt = ""
  search_timer : any
  paginator : any
  
  sort_campaign_current = 0 //all
  user_details : user_info = null
  campaigns : CAMPAIGNS[] = null
  
  filter_arr : string[] = []
  
  
  constructor(
    private http : HttpClient,
    private loginservice : LoginService,
    private configservice : ConfigService
    ) {
      this.user_details = this.loginservice.userDetails()
      this.sortStatusCampaigns(this.sort_campaign_current)
    }
    
    ngOnInit() {
    }
    
    fetchData(filter : string = "") : void {
      this.campaigns = null
      this.http.get<any>(this.configservice.CONFIG.api_endpoints.campaigns + filter, { headers : this.configservice.prepareHeader(this.user_details)})
      .subscribe((response) => {
        if (response.error_code > 0) {
          //Something is not right
          switch (response.error_code) {
            case 301: //Invalid request
            case 302: //Missing token
            case 304: //Invalid token
            this.loginservice.logUserOut()
            break;          
            default:
            window.alert(response.error)
            break;
          }
          return
        }
        this.campaigns = response.campaigns
      },(e : HttpErrorResponse) => {
        window.alert("Service is temporarily not available")
      })
    }
    
    sortStatusCampaigns(to : number) : void {
      this.sort_campaign_current = to
      if (this.sort_campaign_current == 0) {
        this.filter_arr = this.filter_arr.filter(c => c.indexOf('status=') === -1)
      }else {
        this.filter_arr.push(`status=${this.sort_campaign_current}`)
      }
      this.__resolveFilters()
    }
    
    __resolveFilters() : void {
      if (this.filter_arr.length === 0) {
        this.fetchData()
        return
      }
      this.fetchData(`?${this.filter_arr.join('&')}`)
    }
    
    howManyMoreDaysTillStart(campaign : CAMPAIGNS) : string {
      return "2 days"
    }
    
    howManyMoreDaysTillEnd(campaign : CAMPAIGNS) : string {
      return "2 days"
    }
    
    pageChanged(event : any) : void {
      window.scroll(0,0)
    }
    
    dateDisplayHandler(campaign : CAMPAIGNS) : string {
      let end = new Date(campaign.duration.end.replace(/-/g, "/"))
      let start = new Date(campaign.duration.start.replace(/-/g, "/"))
      let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]
      
      let _s = start.getDate() + " " + months[start.getMonth()]
      if (start.getFullYear() !== end.getFullYear()) {
        _s +=  " " + start.getFullYear()
      }
      let _e = end.getDate() + " " + months[end.getMonth()] + " " + end.getFullYear()
      return `${_s} - ${_e}`
    }
    
    sortAlphabetically(key = "description") : void {
      let c_dup : CAMPAIGNS[] = JSON.parse(JSON.stringify(this.campaigns))
      c_dup.sort((a,b) => {
        if (a[key] < b[key]) return -1
        if (a[key] > b[key]) return 1
        return 0
      })
      if (JSON.stringify(c_dup) == JSON.stringify(this.campaigns)){
        this.campaigns.reverse()
        return
      }
      this.campaigns = c_dup
    }
    
    sortStatusAlphabetically() : void {
      this.campaigns = this.campaigns.map(c => {
        switch (c.status) {
          case 1:
          c.statusText = "Published"
          break;
          case 2:
          c.statusText = "Processing"
          break;
          case 3:
          c.statusText = "Ongoing"
          break;
          case 4:
          c.statusText = "Archived"
          break;
          default:
          c.statusText = ""
          break;
        }
        return c
      })
      this.sortAlphabetically('statusText')
    }
    
    sortDuration() : void {
      let c_dup : CAMPAIGNS[] = JSON.parse(JSON.stringify(this.campaigns))
      c_dup.sort((a, b) => {
        return new Date(a.duration.end).getTime() - new Date(b.duration.end).getTime()
      })
      if (JSON.stringify(c_dup) == JSON.stringify(this.campaigns)){
        this.campaigns.reverse()
        return
      }
      this.campaigns = c_dup
    }
    
    searchCampaigns(start_time = true) : void {
      clearTimeout(this.search_timer)
      if (start_time) {        
        this.search_timer = setTimeout(() => {
          this.search_campaign_txt = this.search_campaign_txt.trim()
          this.filter_arr = this.filter_arr.filter(c => c.indexOf('search=') === -1)
          if (this.search_campaign_txt != "") {
            this.filter_arr.push(`search=${this.search_campaign_txt}`)
          }
          this.__resolveFilters()
        }, 1000)
      }
    }
    
  }