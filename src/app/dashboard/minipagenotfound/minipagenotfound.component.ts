import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-minipagenotfound',
  templateUrl: './minipagenotfound.component.html',
  styleUrls: ['./minipagenotfound.component.scss']
})
export class MinipagenotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
