import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinipagenotfoundComponent } from './minipagenotfound.component';

describe('MinipagenotfoundComponent', () => {
  let component: MinipagenotfoundComponent;
  let fixture: ComponentFixture<MinipagenotfoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinipagenotfoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinipagenotfoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
