import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CampaignsComponent } from './dashboard/campaigns/campaigns.component';
import { MinipagenotfoundComponent } from './dashboard/minipagenotfound/minipagenotfound.component';
import { CampaigndetailsComponent } from './dashboard/campaigndetails/campaigndetails.component';
import { PerformancesummaryComponent } from './dashboard/performancesummary/performancesummary.component';
import { SitesComponent } from './dashboard/sites/sites.component';
import { AudienceComponent } from './dashboard/audience/audience.component';
import { TimeComponent } from './dashboard/time/time.component';
import { AttributionComponent } from './dashboard/attribution/attribution.component';
import { AdsComponent } from './dashboard/ads/ads.component';
import { FaqComponent } from './dashboard/faq/faq.component';
import { ReportComponent } from './dashboard/report/report.component';
import { LoginComponent } from './login/login.component';
import { LoadingComponent } from './loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    DashboardComponent,
    CampaignsComponent,
    MinipagenotfoundComponent,
    CampaigndetailsComponent,
    PerformancesummaryComponent,
    SitesComponent,
    AudienceComponent,
    TimeComponent,
    AttributionComponent,
    AdsComponent,
    FaqComponent,
    ReportComponent,
    LoginComponent,
    LoadingComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
